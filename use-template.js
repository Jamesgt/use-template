customElements.define('use-template', class extends HTMLElement {

  static get observedAttributes() {
    return ['if', 'for', 'target', 'attr']
  }

  get if() {
    return this.getAttribute('if') || true
  }

  set if(v) {
    this.setAttribute('if', !!v)
  }

  get for() {
    try {
      return JSON.parse(this.getAttribute('for'))
    } catch (error) {
      console.error('Invalid JSON in "for" attribute', this, error)
      return null
    }
  }

  constructor() {
    super()

    this.attachShadow({ mode: 'open' })

    this.render()
  }

  // lifecycle events
  // connectedCallback() {}
  // disconnectedCallback() {}

  attributeChangedCallback(name, oldValue, newValue) {
    this.render()
  }

  render() {
    this.shadowRoot.innerHTML = ''

    if (!eval(this.if)) {
      return
    }

    const selector = this.getAttribute('src')
    const template = document.querySelector(selector)

    const forValue = this.for

    if (!forValue) {
      const clone = template.content.cloneNode(true)
      this.shadowRoot.appendChild(clone)
      return
    }
    if (!Array.isArray(forValue)) {
      for (let i = 0; i < forValue; i++) {
        const clone = template.content.cloneNode(true)
        this.shadowRoot.appendChild(clone)
      }
      return
    }
    for (let elem of forValue) {
      const clone = template.content.cloneNode(true)
      const target = clone.querySelector(this.getAttribute('target'))
      const attributes = this.getAttribute('attr').split('.')
      const lastAttribute = attributes.pop()
      let currentTarget = target
      for (let attribute of attributes) {
        currentTarget = target[attribute]
      }
      currentTarget[lastAttribute] = elem
      this.shadowRoot.appendChild(clone)
    }
  }

})
