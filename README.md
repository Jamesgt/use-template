# use-template.js

A simple web component to allow declarative template instantiation.

## Examples

### Basic usage

From the following decalration

```html
  <template class="example">
    <div class="contentClass">content</div>
  </template>
  <use-template src=".example"></use-template>
```

this is created

```html
<use-template src=".example">
  #shadow-root
    <div class="contentClass">content</div>
</use-template>
```

### Conditional

With the `if` attribute the instancing is based on the attribute's value.

### Loops

With the `for` attribute the template will be instanced the given time.

#### Arrays

If the `for` attribute is present, then `target` and `attr` attributes can be used to customize each instance by assigning different values to an attribute.
